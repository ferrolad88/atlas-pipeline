#!/bin/sh
#SBATCH --err log/snakemake-%A.out
#SBATCH --out log/snakemake-%A.out
#SBATCH --ntasks-per-node=8
#SBATCH --mem=25G
#SBATCH --time=150:00:00
#SBATCH --job-name=Snakemake





###################################################################################################
#---this script submits the Snakemake-aDNA-pipeline execution to the cluster.
###################################################################################################

#getopt needs a date set
date=$(date -I)

#unset parameters
unset usage dryrun config cluster clusterfile pallas plot jobs

#################################################################################################
#-----getopt
#################################################################################################
# define the help

usage='
\n \**************************************************************************************\
\n
\n\t	This script executes the snakemake pipeline for Local Realignment.
\n\t    
\n\t	Default parameters are specified for ancient DNA analysis 
\n\t	Usage: bash Atlas-Pipeline.sh <options>
\n
\n
\n\t	Before running the pipeline:
\n\t	prepare a config-file as described in the documentation (https://bitbucket.org/wegmannlab/atlas-pipeline/wiki/Home)
\n
\n
\n\t	To ensure package continuity, we advise the use of the provided conda environment by running the following command:
\n\t	conda env create --name Atlas-Pipeline_Environment --file environment.yaml
\n\t	
\n\t	Before starting a snakemake session call this environment by executing:
\n\t	conda activate Pipeline_Environment
\n\t	
\n\t	
\n \**************************************************************************************\
\n
\n
\n\t -h	\t	open help page
\n\t\t	
\n\t -d \t  perform a dry-run of the pipeline
\n\t\t	
\n\t -f \t  specify your config-file (required)
\n\t\t      copy the example config-file to a desired location and change the parameters within
\n\t\t	    attention!: do not alter the example.config.yaml file!
\n\t\t	                in case of an update of the pipeline, it will be overwritten!
\n\t\t	
\n\t\t		
\n\t -p \t  make a plot of the DAG structure
\n\t -c \t  run on a slurm cluster
\n\t\t		
\n\t\t\t	cluster file can be provided. Default: cluster.json
\n\t\t		
\n\t -j \t  specify how many jobs should be submitted alongside (only on cluster system). Default: 50
\n\t\t		
\n\t\t
\n \\\ \t citations for used scripts and programms in this pipeline: \t\t \\\
\n \\\ \t\t\t\t\t\t\t\t\t\t \\\
\n \\\ \t This script is available free of use at  \t\t\t\t \\\
\n \\\ \t https://bitbucket.org/wegmannlab/adnapipeline.bam/src/master/  \t\t \\\
\n \\\ \t for questions correspond to ilektra.schulz@unifr.ch  \t\t\t \\\
\n \\\ \t To pull the newest version, run  \t\t\t\t\t \\\
\n \\\ \t git clone https://iSchulz@bitbucket.org/wegmannlab/adnapipeline.bam.git \\\
\n \\\ \t\t\t\t\t\t\t\t\t\t \\\
\n \\\ \t within the script, the following programs, in addition  \t\t \\\
\n \\\ \t to the programs listed in environment_recent.yaml were used:   \t\t \\\
\n \\\ \t GATK: https://software.broadinstitute.org/gatk/ \t\t\t\t\t \\\
\n \\\ \t\t\t\t\t\t\t\t\t\t \\\
\n \\\ \t Snakemake: http://snakemake.readthedocs.io/en/stable/index.html \t \\\
\n \\\ \t\t\t\t\t\t\t\t\t\t \\\
\n \\\ \t No guarantee for the content and results of this script to be bug-free. \\\
\n
\n \**************************************************************************************\
\n 
\n 
\n 
\n 
'
#get options to variables
while getopts ":hdf:c:pj:" OPTION
do
    case $OPTION in
    h)
	    echo -e $usage
	    exit 1
	    ;;

	d)
		echo -e "performing dry-run of the pipeline \n"
		dryrun="yes"
		exec="no"
		;;

	f)
        config=${OPTARG}
        if [[ -z $config ]]; then
			echo -e $usage
			echo -e "ERROR: you must specify a config file with the option -f !!"
			exit 1
		fi
        if [[ -f $config ]]; then
			echo -e "found config file: ${config} \n"
		else
			echo -e "ERROR: config-file not found!"
			exit 1
		fi
		;;

	c)
        cluster="yes"
        if test "$OPTARG" = "$(eval echo '$'$((OPTIND - 1)))"; then
        	    clusterfile="$OPTARG" 
                echo "performing analysis on cluster with parameters from $clusterfile" 
            else
                OPTIND=$((OPTIND - 1))
                clusterfile=cluster.json
                echo "performing analysis on cluster with default $clusterfile ."
        fi 
	    ;;

	p)	    
	    echo -e "plotting DAG in plots \n"
	    plot="yes"
	    ;;

	j)
		jobs=${OPTARG}
	    echo -e "executing $jobs jobs in parallel (option -j) \nonly applied on cluster systems (option -c)"
        ;;
	\?)
		echo -e "ERROR: no valid parameters defined! \n\n"
        echo -e $usage
        exit 1
        ;;

    :)
	    case "$OPTARG" in
	     	c)
            	clusterfile=cluster.json
            	cluster="yes"
                echo "performing analysis on cluster with default $clusterfile .."
                ;;
			*) 
				echo "option requires an argument -- $OPTARG" 
				;;
		esac 
		;;
    esac
done 


#check defaults and inputs




#detect mode

mode=$(grep "runScript" $config | grep -v "#" | awk '{print $2}')

case $mode in 
	Gaia) 	
		echo "you are executing the Gaia pipeline (runScript: Gaia)" 
	;;

	Rhea-targetCreator)
        mkdir -p resources
        if [ -f resources/contigs.txt ]; then
        	echo "Using already excisting file resources/contigs.txt. Delete, if you want Atlas-Pipeline to retrieve the contigs again."
        else
         	file=$(grep "^targets:" $config | grep -v "#" | awk '{print $2}')
        	line=$(grep -v Target $file | grep -v "#" | head -n1); sam=$(echo $line | awk '{print $1}') ; path=$(echo $line | awk '{print $2}')
		    echo "Reading contigs from $sam header to resources/contigs.txt. You can provide this file yourselve to resources/contigs.txt ..."
        	echo "chr" > resources/contigs.txt
            samtools view -H ${path}${sam}.bam | grep '^@SQ' | awk '{print $2}' | sed 's/SN://g' >> resources/contigs.txt
		fi
 	;;

	Rhea-localReal)
        mkdir -p resources
	    if [ -f resources/contigs.txt ]; then
			echo "Using already excisting file resources/contigs.txt. Delete, if you want Atlas-Pipeline to retrieve the contigs again."
        else
         	file=$(grep "^sample_file:" $config | grep -v "#" | awk '{print $2}')
        	line=$(grep -v Target $file | grep -v "#" | head -n1); sam=$(echo $line | awk '{print $1}') ; path=$(echo $line | awk '{print $2}')
		    echo "Reading contigs from $sam header to resources/contigs.txt. You can provide this file yourselve to resources/contigs.txt ..."
        	echo "chr" > resources/contigs.txt
            samtools view -H ${path}${sam}.bam | grep '^@SQ' | awk '{print $2}' | sed 's/SN://g' >> resources/contigs.txt
		fi 
	;;

	Perses) 
		
	;;
esac



#################################################################################################
#-----for LocalRealignment
#################################################################################################




#################################################################################################
#-----for ATLAS-pipeline
#################################################################################################

mkdir -p plots

#unlock history
snakemake -s Snakefile --configfile $config --unlock --rerun-incomplete


# make plots
if [[ $plot == "yes" ]]; then
    snakemake -s Snakefile --configfile $config  --dag --rerun-incomplete | dot -Tpdf > plots/pipeline-${tag}.pdf
fi

#dryrun
if [[ $dryrun == "yes" ]]; then	
	snakemake -s Snakefile --configfile $config  -np --rerun-incomplete --latency-wait 360 -j $jobs
else
	#cluster
	if [[ $cluster == "yes" ]]; then
		#define job number default
		if [ -z $jobs ]; then
		jobs=50
		echo "Executing $jobs jobs in parallel (default)"
		fi

		#check for clusterfile
		if [[ ! -f $clusterfile ]]; then 
				echo -e "ERROR: no cluster file present! Looking for $clusterfile."
				exit 1
		fi
			snakemake -s Snakefile --configfile $config -p --rerun-incomplete --latency-wait 360 -j $jobs --cluster-config $clusterfile --cluster "sbatch --mem={cluster.mem} --time {cluster.time} --output {cluster.out} --error {cluster.err} --job-name {cluster.name} --cpus-per-task {cluster.cpus-per-task}"
	#front-machine
	else
		tag=$(date +"%T"| sed 's/:/-/g')
		snakemake -s Snakefile --configfile $config -p --rerun-incomplete --latency-wait 360 2>&1 | tee logs/snakemake-${tag}.txt
	fi
fi


