#import libraries
import pandas as pd
from pathlib import Path
import os

#define configfile
#configfile: "config_LocalReal.yaml"

RUN = config['runScript']


##################################################################################
#------------------------------------GAIA----------------------------------------#
##################################################################################
if (RUN == "Gaia"):
    #load librarylist as dataframe
    df = pd.read_csv(config['sample_file'], sep='\t', index_col='File', comment='#')

    include: "scripts/dependencies_Fastq.snakefile"

    #this is the rule snakemake calls by default.
    rule all:
        input:
            "Results/1.FASTQ/FILE_COUNTS",
            "Results/1.FASTQ/SAM_COUNTS",
            expand("Results/1.FASTQ/summaries/{sample}.summary.txt", zip, sample=SAMPLE)#,
            #expand("Results/1.FASTQ/summaries/{file}.summary.txt", zip, file=FILE)

    include: "scripts/rawcount.snakefile"
    include: "scripts/trim.snakefile"
    include: "scripts/align.snakefile"
    include: "scripts/merge.snakefile"
    include: "scripts/counts.snakefile"


    rule summary_file:
        input:
            merged="Results/1.FASTQ/x.SamStats/{sample}_BamDiagnostics.log",
            BamD1=lambda wildcards: expand('Results/1.FASTQ/y.FileCounts/{file}.counts', file = df.index[ df.Sample == wildcards.sample]),
            fastqc1_pre=lambda wildcards: expand('Results/1.FASTQ/02.fastqc/{file}_R1_001_fastqc.html', file = df.index[ df.Sample == wildcards.sample])
        output:
            file="Results/1.FASTQ/summaries/{sample}.summary.txt"
        shell:
            """
            echo -e '{input.fastqc1_pre} {input.BamD1} {input.merged}' > {output.file}
            """



##################################################################################
#------------------------------RHEA - Target Creator-----------------------------#
##################################################################################
if (RUN == "Rhea-targetCreator"):
    #load samplelist as dataframe
    #df = pd.read_csv(config['sample_file'], sep='\t', index_col='Sample', comment='#')

    ####define targets
    ta = pd.read_csv(config['targets'], sep='\t', index_col='Target', comment='#')
    ####define contigs

    #define variables
    TARGET=ta.index
    TAR_PATH = ta["Path"]
    #SAMPLES = df.index
    #BAM_PATH = df["Path"]

    PAR=config["parallelizeChrom"]

    if ( config['GATK'] == None ):
        GATK="echo ''"
    else:
        GATK=config['GATK']


    if (PAR == "T"):
    ####this is the rule snakemake calls by default.
        ch = pd.read_csv("resources/contigs.txt", sep='\t', index_col='chr', comment='#')
        CHROM=ch.index
        rule all:
            input:
                "Results/2.LOCREAL/TargetIntervals/Contigs.interval_list"
    elif (PAR == "F"):
        ####this is the rule snakemake calls by default.
        rule all:
            input:
                "Results/2.LOCREAL/TargetIntervals/all.guidance.interval_list"


    include: "scripts/TargetCreator.snakefile"


##################################################################################
#------------------------------RHEA - Local Realigner----------------------------#
##################################################################################
if (RUN == "Rhea-localReal"):
    #load samplelist as dataframe
    df = pd.read_csv(config['sample_file'], sep='\t', index_col='Sample', comment='#')
    
    PAR=config["parallelizeChrom"]


    if (PAR == "T"):
        if (config['contigs'] == "From_target_creator"):
            ch = pd.read_csv("Results/2.LOCREAL/TargetIntervals/Contigs.interval_list", sep='\t', index_col='chr', comment='#')
        if (config['contigs'] == "external"):
            ch = pd.read_csv(config['contigList'], sep='\t', index_col='chr', comment='#')
        CHROM=ch.index

    if (PAR == "F"):
        def int_list():
            if (config['contigs'] == "From_target_creator"):
                return ['Results/2.LOCREAL/TargetIntervals/all.guidance.interval_list']
            if (config['contigs'] == "external"):
                return config['intervalList']


    #define variables
    SAMPLES = df.index
    BAM_PATH = df["Path"]

    GUIDANCE=config['bamsGuidance']
    
    ####this is the rule snakemake calls by default.
    rule all:
        input:
            expand("Results/2.LOCREAL/final/{sample}.bam", sample=SAMPLES)

    if (PAR == "T"):
        include: "scripts/LocalReal_parr.snakefile"
    if (PAR == "F"):
        include: "scripts/LocalReal_noParr.snakefile"


##################################################################################
#-----------------------------------PERSES---------------------------------------#
##################################################################################
if (RUN == "Perses"):
    #load samplelist as dataframe
    df = pd.read_csv(config['sample_file'], sep='\t', index_col='Sample', comment='#')

#    include: "scripts/dependencies_ATLAS.snakefile"
    SAMPLES = df.index
    BAM_PATH = df["Path"]
    PMD = config['PMD']

    ####this is the rule snakemake calls by default.
    rule all:
        input:
            expand("Results/3.Perses/summaries/{sample}.summary.txt", zip, sample=SAMPLES)


    include: "scripts/BAMD.snakefile"
    include: "scripts/splitMerge.snakefile"
    include: "scripts/PMD.snakefile"
    include: "scripts/recal.snakefile"

    inputFiles = ["Results/3.Perses/recal/{sample}_recalibrationEM.txt", "Results/3.Perses/BAMDiagnostics1/{sample}_approximateDepth.txt", "Results/3.Perses/BAMDiagnostics2/{sample}_approximateDepth.txt"]
    
    if (config['recalBAM'] == "T"):
        inputFiles.append("Results/3.Perses/recalBAM/{sample}_recalibrated.bam")

    rule summary:
        input:
            inputFiles
        output:
            "Results/3.Perses/summaries/{sample}.summary.txt"
        shell:
            """
            echo {input} > {output}
            """



##################################################################################
#-----------------------------------PALLAS---------------------------------------#
##################################################################################
if (RUN == "Pallas"):
    df = pd.read_csv(config['sample_file'], sep='\t', index_col='Sample', comment='#')

    SAMPLES = df.index
    BAM_PATH = df["Path"]


    rule all:
        input:
            expand("Results/4.Pallas/summaries/{sample}.summary.txt", zip, sample=SAMPLES)

    include: "scripts/calling.snakefile"
    include: "scripts/glf.snakefile"
    include: "scripts/estimateTheta.snakefile"
    include: "scripts/additionalAnalysis.snakefile"


    inputFiles = []

    if (config['sex'] == "T"):
        inputFiles.append("Results/4.Pallas/sex/{sample}.Sex")

    if (config['call'] == "Bayes"):
        inputFiles.append("Results/4.Pallas/call/callBayes/{sample}_MaximumAPosteriori.vcf.gz")
    elif (config['call'] == "MLE"):
        inputFiles.append("Results/4.Pallas/call/callMLE/{sample}_MaximumLikelihood.vcf.gz")
    elif (config['call'] == "AllelePresence"):
        inputFiles.append("Results/4.Pallas/call/AllelePresence/{sample}_AllelePresence.vcf.gz")

    
    if (config['glf'] == "T"):
        inputFiles.append("Results/4.Pallas/GLF/{sample}.glf.gz")

    if (config['theta'] == "global"):
        inputFiles.append("Results/4.Pallas/theta/genomeWide/{sample}_theta_estimates.txt.gz")
    elif (config['theta'] == "window"):
        inputFiles.append("Results/4.Pallas/theta/wind/{sample}_theta_estimates.txt.gz")

    rule summary:
        input:
            inputFiles
        output:
            "Results/4.Pallas/summaries/{sample}.summary.txt"
        shell:
            """
            echo {input} > {output}
            """

