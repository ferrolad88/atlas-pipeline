# README for a flexible aDNA-pipeline using Atlas and Snakemake

-- this pipeline is still under construction --  please contact ilektra.schulz@unifr.ch for questions, suggestions, bugs, etc....

>                           ___
>                             {-)   |\
>                        [m,].-"-.   /
>      [][__][__]          \(/\__/\)/
>      [__][__][__][__]~~~~   |  |
>      [][__][__][__][__][]  /   |
>      [__][__][__][__][__] | /| |
>      [][__][__][__][__][] | || |  ~ ~ ~  
>      [__][__][__][__][__] __,__,  \__/  
> ejm


This is a pipeline, used to analyze bamfiles deriving from ancient DNA, mainly using the program [ATLAS](https://bitbucket.org/wegmannlab/atlas/wiki/Home).

## BEFORE RUNNING PIPELINE

- Consult the [pipeline's wiki](https://bitbucket.org/wegmannlab/atlas-pipeline/wiki). 

- This pipeline is constructed using the workflow management system tool [Snakemake](https://snakemake.readthedocs.io/en/stable/).

- To ensure package-continuity, a [conda-environment](https://conda.io/docs/user-guide/tasks/manage-environments.html) is provided alongside. You can build an environment using the file `environmentXX.yaml` from within this package, or ensure that all listed programs and packages are installed on your system.

- Cluster compatibility for SLURM cluster systems is included. You can change your [...]
