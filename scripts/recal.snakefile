
#def recal_file():
#    if (config['recalFile'] == "calculate"):
#        return 'Results/3.Perses/recal/{sample}_recalibrationEM.txt'        
#    else:
#        return config['recalFile']

rule recal_inputFile:
    params:
        poolRG="readgroups/{sample}.poolRG_recal.txt",
        splitmergeRG="readgroups/{sample}_readgroups.txt"        
    output:
        recal_input="Results/3.Perses/readgroups/{sample}_recal_inputfile.txt"
    shell:
        """
        touch  {output.recal_input}
        if [[ -f "{params.poolRG}" ]]; then
            echo -e "reading readgroups to pool from {params.poolRG}"
            while read line; do 
            newline=""
            for i in ${{line}}; do
                this=$(cat {params.splitmergeRG} | awk -v var="${{i}}" '{{if ($1==var && $2=="single" && ($3)) {{print $1, " " , $1"_truncated"}} else if ($1==var && $2=="paired") {{print $1}} }}')
                newline="$newline $this"
            done
            if [[ $(echo $newline | wc -w) -gt 1 ]]; then echo $newline  >>  {output.recal_input}; fi
            done < {params.poolRG}        
        else
            awk '{{ if ($2=="single" && ($3)) {{print $1, " " , $1"_truncated"}} else if ($2=="paired") {{print $1}} }}' {params.splitmergeRG} >> {output.recal_input}
        fi
        """


#if (RECAL == "RG-file") or (RECAL == "NO-file"):
rule recal:
    input:
        bam="Results/3.Perses/splitMerge/{sample}_mergedReads.bam",
        pmd="Results/3.Perses/PMDestimation/{sample}_PMD_input_Empiric.txt",
        poolRG="Results/3.Perses/readgroups/{sample}_recal_inputfile.txt"
    params:
        prefix="Results/3.Perses/recal/{sample}"
    output:
        "Results/3.Perses/recal/{sample}_recalibrationEM.txt"
    message:
        "recalibrating quality scores based on PMD patterns and ultra-conserved regions on {wildcards.sample}"
    shell:
        """
        {config[atlas]} \
            task=recal \
            bam={input.bam} \
            pmdFile={input.pmd} \
            out={params.prefix} \
            {config[recalFormat]}={config[recalInput]} \
            poolReadGroups={input.poolRG} \
            model=qualFuncPosSpecificContext  {config[atlasParams]}  {config[recalParams]}
        """


rule recalBAM:
    input:
        bam="Results/3.Perses/splitMerge/{sample}_mergedReads.bam",
        pmd="Results/3.Perses/PMDestimation/{sample}_PMD_input_Empiric.txt",
        recal="Results/3.Perses/recal/{sample}_recalibrationEM.txt"
    params:
        prefix="Results/3.Perses/recalBAM/{sample}"
    output:
        "Results/3.Perses/recalBAM/{sample}_recalibrated.bam"
    message:
        "Creating recalibrated bamfile on {wildcards.sample}"
    shell:
        """
        {config[atlas]} \
            task=BAMUpdateQualities \
            bam={input.bam} \
            pmdFile={input.pmd} \
            recal={input.recal} \
            withPMD \
            fasta={config[ref]} \
            out={params.prefix} {config[atlasParams]} {config[recalBamParams]}
        """


