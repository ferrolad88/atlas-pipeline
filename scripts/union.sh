#!/bin/bash

#module add UHTS/Analysis/picard-tools/2.9.0;

#$1=input
#$2=guidance.interval_list
#$3=chrom
#$4=header

guidance=/data/projects/p243_ancientdna_unifr/Temporary/Carlos/Realignment/10X/Pipeline/Intervals/24S

##sample1
awk -F '-' 'BEGIN{OFS = "-"} {print $1,$1,$2}' $1 | sed 's/-$//'|sed 's/-.*-/-/'|sed 's/-.*:/-/'|sed 's/:/\t/' |sed 's/-/\t/'|sed 's/$/\t+/' > ${1/intervals/interval_list}
paste ${1/intervals/interval_list} $1 > ${1/intervals/interval_list2} 
cat $4 ${1/intervals/interval_list2} > ${1/intervals/interval_list}
rm ${1/intervals/interval_list2}


picard IntervalListTools ACTION=UNION SORT=true  UNIQUE=true  I=${guidance}/mills_1000G_guidance_chr${3}.interval_list  I=${1/intervals/interval_list} O=$2


sed '/@/d' $2 | grep "|" | cut -f1-3|sed 's/\t/:/'|sed 's/\t/-/' >  ${2/interval_list/interval_list2}
sed '/@/d' $2 | grep -v "|"|cut -f5|cat - ${2/interval_list/interval_list2} | sed 's/:/\t/' |sed 's/-/\t/'|sort -nk2,2 | sed 's/\t/:/' | sed 's/\t/-/' > ${2/interval_list/intervals}

#rm ${2/interval_list/interval_list2}


