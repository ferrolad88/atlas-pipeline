rule CreateHeader:
    output:
        header="Results/2.LOCREAL/init/header.txt"
    shell:
        """
        line=$(grep -v Target {config[targets]} | grep -v "#" | head -n1); sam=$(echo $line | awk '{{print $1}}') ; path=$(echo $line | awk '{{print $2}}')
        samtools view -H ${{path}}${{sam}}.bam | grep '^@SQ\|^@HD' > {output.header} 
        """

if (PAR == "T"):

    rule TargetCreatorT:
        input:
            bam=lambda wildcards: str(Path(TAR_PATH[wildcards.target])/f"{wildcards.target}.bam")
        output:
            temp("Results/2.LOCREAL/Targets/{target}-{chrom}-.intervals")
        params:
            gatk=GATK
        shell:
            """
            {params.gatk}
            GenomeAnalysisTK \
               -T RealignerTargetCreator \
               -R {config[ref]} \
               -I {input.bam} \
               -L {wildcards.chrom} \
               -o {output} \
               -nt 4 \
               {config[known1]} {config[known2]}
            """

    rule union0T:
        input:
            interval="Results/2.LOCREAL/Targets/{target}-{chrom}-.intervals",
            header="Results/2.LOCREAL/init/header.txt"
        output:
            temp=temp("Results/2.LOCREAL/Targets/{target}-{chrom}-.interval_list"),
            temp1=temp("Results/2.LOCREAL/Targets/{target}-{chrom}-.interval_list2")
        shell:
            """
            awk -F '-' 'BEGIN{{OFS = "-"}} {{print $1,$1,$2}}' {input.interval} | sed 's/-$//'|sed 's/-.*-/-/'|sed 's/-.*:/-/'|sed 's/:/\\t/' |sed 's/-/\\t/'|sed 's/$/\\t+/' > {output.temp}
            paste {output.temp} {input.interval} > {output.temp1}
            cat {input.header} {output.temp1} > {output.temp}
            """

    rule union1T:
        input:
            expand("Results/2.LOCREAL/Targets/{target}-{{chrom}}-.interval_list", target=TARGET) #$1
        output:
            temp2="Results/2.LOCREAL/TargetIntervals/{chrom}-.guidance.interval_list"
        shell:
            """
            export _JAVA_OPTIONS='-Xmx120G'
            in=
            for i in {input}; do 
                this="I=${{i}}";
                echo ${{this}}; 
                in2="${{in}} ${{this}}"
                in=${{in2}}
            done
            echo ${{in}}
            set +e
            picard IntervalListTools \
                ACTION=UNION \
                SORT=true \
                UNIQUE=true \
                ${{in}} \
                O={output.temp2}
            exitcode=$?
            if [ $exitcode -eq 1 ]; then exit 1; else exit 0; fi
            """

    rule CreateListT:
        input:
            #lambda wildcards: expand("Results/2.LOCREAL/TargetsIntervals/{chrom}.intervals", chrom=CHROM)
            lambda wildcards: expand("Results/2.LOCREAL/TargetIntervals/{chrom}-.guidance.interval_list", chrom=CHROM)
        output:
            "Results/2.LOCREAL/TargetIntervals/Contigs.interval_list"
        shell:
            """
            echo -e "chr\\tint" > {output}
            for i in {input}; do c=$(ls ${{i}} | rev | cut -d/ -f1 | rev |sed 's/-.guidance.interval_list//g' ); echo -e "${{c}}\\t${{i}}" >> {output}; done
            """


if (PAR == "F"):

    rule TargetCreatorT:
        input:
            bam=lambda wildcards: str(Path(TAR_PATH[wildcards.target])/f"{wildcards.target}.bam")
        output:
            temp("Results/2.LOCREAL/Targets/{target}.intervals")
        params:
            gatk=GATK
        shell:
            """
            {params.gatk}
            GenomeAnalysisTK \
               -T RealignerTargetCreator \
               -R {config[ref]} \
               -I {input.bam} \
               -o {output} \
               -nt 4 \
               {config[known1]} {config[known2]}
            """

    rule union0T:
        input:
            interval="Results/2.LOCREAL/Targets/{target}.intervals",
            header="Results/2.LOCREAL/init/header.txt"

        output:
            temp=temp("Results/2.LOCREAL/Targets/{target}.interval_list"),
            temp1=temp("Results/2.LOCREAL/Targets/{target}.interval_list2")
        shell:
            """
            awk -F '-' 'BEGIN{{OFS = "-"}} {{print $1,$1,$2}}' {input.interval} | sed 's/-$//'|sed 's/-.*-/-/'|sed 's/-.*:/-/'|sed 's/:/\\t/' |sed 's/-/\\t/'|sed 's/$/\\t+/' > {output.temp}
            paste {output.temp} {input.interval} > {output.temp1}
            cat {input.header} {output.temp1} > {output.temp}
            """

    rule union1T:
        input:
            expand("Results/2.LOCREAL/Targets/{target}.interval_list", target=TARGET) #$1
        output:
            temp2="Results/2.LOCREAL/TargetIntervals/all.guidance.interval_list"
        shell:
            """
            export _JAVA_OPTIONS='-Xmx120G'
            in=
            for i in {input}; do 
                this="I=${{i}}";
                echo ${{this}}; 
                in2="${{in}} ${{this}}"
                in=${{in2}}
            done
            echo ${{in}}
            set +e
            picard IntervalListTools \
                ACTION=UNION \
                SORT=true \
                UNIQUE=true \
                ${{in}} \
                O={output.temp2}
            exitcode=$?
            if [ $exitcode -eq 1 ]; then exit 1; else exit 0; fi
            """