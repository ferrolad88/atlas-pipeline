if (SEQUENCE == "single"):
    rule finalBAM_single:
        input:
            bam="analysis/splitRG/{sample}_splitRG.bam",
            bai="analysis/splitRG/{sample}_splitRG.bam.bai"
        output:
            bam="analysis/final_BAM/{sample}_final.bam",
            bai="analysis/final_BAM/{sample}_final.bam.bai"
        message:
            "copy single-end Bamfile to final location {wildcards.sample}"
        conda:
            "environment2.yaml"
        shell:
            """
            cp {input.bam} {output.bam}
            cp {input.bai} {output.bai}
            """

if (SEQUENCE == "mixed") or (SEQUENCE == "paired"):

    rule finalBAM_paired:
        input:
            bam="analysis/recalBAM/{sample}_mergedReads.bam",
            bai="analysis/recalBAM/{sample}_mergedReads.bam.bai"
        output:
            bam="analysis/final_BAM/{sample}_final.bam",
            bai="analysis/final_BAM/{sample}_final.bam.bai"
        message:
            "copy mixed or paired-end Bamfile to final location {wildcards.sample}"
        conda:
            "environment2.yaml"
        shell:
            """
            cp {input.bam} {output.bam}
            cp {input.bai} {output.bai}
            """
