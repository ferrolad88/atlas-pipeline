rule recalBAM:
    input:
        bam=bamfile(),
        recal=recal_file()
    params:
        prefix="analysis/recalBAM/{sample}"
    output:
        "analysis/recalBAM/{sample}_recalibrated.bam",
        "analysis/recalBAM/{sample}_recalibrated.bam.bai",
        "analysis/recalBAM/{sample}_recalBAM.log"
    message:
        "creating recalibrated bamfile {wildcards.sample}"
    conda:
        "environment2.yaml"
    shell:
        "{config[atlas]} task=recalBAM bam={input.bam} recal={input.recal} fasta={config[ref]} out={params.prefix} logFile={params.prefix}_recalBAM.log verbose dontFilterReadsLongerFragment"
