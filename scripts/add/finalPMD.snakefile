if (SEQUENCE == "mixed") or (SEQUENCE == "paired"):
    rule finalPMD_paired:
        input:
            bam="analysis/recalBAM/{sample}_mergedReads.bam"
        params:
            prefix="analysis/final_PMD/{sample}"
        output:
            "analysis/final_PMD/{sample}_PMD_input_Empiric.txt",
            "analysis/final_PMD/{sample}_PMD_input_Exponential.txt",
            "analysis/final_PMD/{sample}_PMD_Table_counts.txt",
            "analysis/final_PMD/{sample}_PMD_Table.txt",
            "analysis/final_PMD/{sample}_PMDestimation.log"
        message:
            "final PMD-estimation on Bamfile with merged paired-end reads {wildcards.sample}"
        conda:
            "environment2.yaml"
        shell:
            "{config[atlas]} task=estimatePMD bam={input.bam} fasta={config[ref]} out={params.prefix} logFile={params.prefix}_PMDestimation.log verbose"

