
rule merge:
    input:
        bam=lambda wildcards: expand('Results/1.FASTQ/08.MkDup_per_lib/{file}.Mkdup.bam', file = df.index[ df.Sample == wildcards.sample]),
        bai=lambda wildcards: expand('Results/1.FASTQ/08.MkDup_per_lib/{file}.Mkdup.bam.bai', file = df.index[ df.Sample == wildcards.sample])
    output:
        temp("Results/1.FASTQ/09.merge/{sample}.bam")
    shell:
        """
        if [ $(echo {input.bam}|wc -w) == 1 ];then
        echo -e 'copying {input.bam} to {output}'
        cp {input.bam} {output}
        else
        echo -e 'input is {input.bam} \n output is {output}'
        samtools merge -f {output} {input.bam}
        fi
        """

rule MkDup_sam:
    input:
        bam="Results/1.FASTQ/09.merge/{sample}.bam"
    output:
        bam="Results/1.FASTQ/10.MkDup_per_sample/{sample}.Mkdup.bam"
    shell:
        """
        export _JAVA_OPTIONS='{config[Xmx]}' 
        set +e
        picard MarkDuplicates INPUT={input.bam} OUTPUT={output.bam} METRICS_FILE=/dev/null REMOVE_DUPLICATES=false AS=true VALIDATION_STRINGENCY=SILENT TMP_DIR=tmp/ TAGGING_POLICY=All
        exitcode=$?
        if [ $exitcode -eq 1 ]; then exit 1; else exit 0; fi
        """

rule MkDup_index2:
    input:
        bam="Results/1.FASTQ/10.MkDup_per_sample/{sample}.Mkdup.bam"
    output:
        index="Results/1.FASTQ/10.MkDup_per_sample/{sample}.Mkdup.bam.bai"
    shell:
        """
        samtools index {input.bam}
        """
