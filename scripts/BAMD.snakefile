rule bamdiagnostics1:
    input:
        bam=lambda wildcards: str(Path(BAM_PATH[wildcards.sample])/f"{wildcards.sample}.bam")
    params:
        prefix="Results/3.Perses/BAMDiagnostics1/{sample}"
    output:
        "Results/3.Perses/BAMDiagnostics1/{sample}_approximateDepth.txt",
        "Results/3.Perses/BAMDiagnostics1/{sample}_fragmentStats.txt",
        "Results/3.Perses/BAMDiagnostics1/{sample}_MQ.txt",
        "Results/3.Perses/BAMDiagnostics1/{sample}_readLength.txt"
    message:
        "running BamDiagnostics...{wildcards.sample}"
    shell:
        "{config[atlas]} task=BAMDiagnostics bam={input.bam} out={params.prefix} filterSoftClips"


rule bamdiagnostics2:
    input:
        bam="Results/3.Perses/splitMerge/{sample}_mergedReads.bam"
    params:
        prefix="Results/3.Perses/BAMDiagnostics2/{sample}"
    output:
        "Results/3.Perses/BAMDiagnostics2/{sample}_approximateDepth.txt",
        "Results/3.Perses/BAMDiagnostics2/{sample}_fragmentStats.txt",
        "Results/3.Perses/BAMDiagnostics2/{sample}_MQ.txt",
        "Results/3.Perses/BAMDiagnostics2/{sample}_readLength.txt"
    message:
        "running BamDiagnostics...{wildcards.sample}"
    shell:
        "{config[atlas]} task=BAMDiagnostics bam={input.bam} out={params.prefix} filterSoftClips"
