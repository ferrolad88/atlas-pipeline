if (SEQUENCE == "single"):
    rule trim:
        input:
            r1=lambda wildcards: str(Path(FQ_PATH[wildcards.file]) / f"{wildcards.file}_R1_001.fastq.gz")
        params:
            ad1=config["AdapterSequence1"],
            qual=config["qualityFilter"],
            leng=config["lengthFilter"]
        output:
            trimmed=temp("Results/1.FASTQ/03.trimmed/{file}_R1_001_trimmed.fq.gz"),
            fastqc2="Results/1.FASTQ/02.fastqc/{file}_R1_001_trimmed_fastqc.html",
            report=temp("Results/1.FASTQ/03.trimmed/{file}_R1_001.fastq.gz_trimming_report.txt")
        threads: config['threads']
        shell:
            """
            test=$(echo {params.ad1})
            echo "applying adapter sequence: ${{test}}"
            if [[ ${{test}} == "default" ]]; then adapter=" " ; else adapter=$(echo "-a ${{test}}"); fi
            trim_galore \
            -j {threads} \
            -o Results/1.FASTQ/03.trimmed/ \
            -q {params.qual} \
            --fastqc \
            --fastqc_args '-k7 --outdir Results/1.FASTQ/02.fastqc/' \
            --length {params.leng}  \
            $adapter \
            {input.r1}
            """

else:
    rule trim:
        input:
            r1=lambda wildcards: str(Path(FQ_PATH[wildcards.file]) / f"{wildcards.file}_R1_001.fastq.gz"),
            r2=lambda wildcards: str(Path(FQ_PATH[wildcards.file]) / f"{wildcards.file}_R2_001.fastq.gz")
            #r1=infast()
        params:
            ad1=config["AdapterSequence1"],
            ad2=config["AdapterSequence2"],
            qual=config["qualityFilter"],
            leng=config["lengthFilter"]
        output:
            trimmed1=temp("Results/1.FASTQ/03.trimmed/{file}_R1_001_val_1.fq.gz"),
            fastqc1_post="Results/1.FASTQ/02.fastqc/{file}_R1_001_val_1_fastqc.html",
            report1=temp("Results/1.FASTQ/03.trimmed/{file}_R1_001.fastq.gz_trimming_report.txt"),
            trimmed2=temp("Results/1.FASTQ/03.trimmed/{file}_R2_001_val_2.fq.gz"),
            fastqc2_post="Results/1.FASTQ/02.fastqc/{file}_R2_001_val_2_fastqc.html",
            report2=temp("Results/1.FASTQ/03.trimmed/{file}_R2_001.fastq.gz_trimming_report.txt")
        threads: config['threads']
        shell:
            """
            test1=$(echo {params.ad1})
            echo "applying adapter sequence: ${{test1}}"
            if [[ ${{test1}} == "default" ]]; then adapter1=" " ; else adapter1=$(echo "-a ${{test1}}"); fi
            test2=$(echo {params.ad2})
            echo "applying reverse adapter sequence: ${{test2}}"
            if [[ ${{test2}} == "default" ]]; then adapter2=" " ; else adapter2=$(echo "-a2 ${{test2}}"); fi
            trim_galore \
            -j {threads} \
            --paired \
            -o Results/1.FASTQ/03.trimmed/ \
            -q {params.qual} \
            --fastqc \
            --fastqc_args '-k7 --outdir Results/1.FASTQ/02.fastqc/' \
            --length {params.leng}  \
            --retain_unpaired $adapter1 $adapter2 \
            {input.r1} {input.r2}
            """

