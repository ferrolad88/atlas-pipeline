##dependency-file for ATLAS workflow

#define variables
SAMPLES = df.index
BAM_PATH = df["Path"]
RECAL = config['recal']
#SEQUENCE = config['sequence']
PMD = config['PMD']
CHR = config['chr']
#SPLIT = config['split']
#THETA = config['theta']

####define bamfile input for splitting readgroups
# def bamToSplit():
#     if (SEQUENCE == "single"):
#         return lambda wildcards: str(Path(BAM_PATH[wildcards.sample])/f"{wildcards.sample}.bam")
#     if (SEQUENCE == "mixed"):
#         return ['Results/3.ATLAS/mergedReads/{sample}_mergedReads.bam']

####define bamfile input of PMD and recal
# def bamfile():
#     if ((SEQUENCE == "single") or (SEQUENCE == "mixed")) and (SPLIT == "T"):
#         return ['Results/3.ATLAS/splitRG/{sample}_splitRG.bam']
#     elif ((SEQUENCE == "single") or (SEQUENCE == "mixed")) and (SPLIT == "F"):
#         return lambda wildcards: str(Path(BAM_PATH[wildcards.sample])/f"{wildcards.sample}.bam")
#     else:
#         return ['Results/3.ATLAS/mergedReads/{sample}_mergedReads.bam']

####define recal-file location
def recal_file():
    if (RECAL == "RC-file"):
        return config['recalRC']
    else:
        return 'Results/3.ATLAS/recal/{sample}_recalibrationEM.txt'
####define PMD-file
def pmd_file():
    if (PMD == "NO-PMD"):
        return ['Results/3.ATLAS/blankPMD/{sample}_PMD_blank_Empiric.txt']
    elif (PMD == "poolRG") or (PMD == "separate"):
        return ['Results/3.ATLAS/PMDestimation/{sample}_PMD_input_Empiric.txt']

########additional input-files based on config-file: ############

####define paired-end readgroups in mixed samples
# def pairedRG():
#     if (SEQUENCE == "paired"):
#         return [' ']
#     if (SEQUENCE == "mixed"):
#         return 'readgroups/{sample}_peRG.txt'

####define file with readgroups to pool for PMD
 # def readgroups_PMD():
 #     if (PMD == "poolRG"):
 #         return 'poolReadGroups=readgroups/{sample}.poolRG_pmd.txt'
 #     if (PMD == "separate"):
 #         return [' ']

####define file with readgroups to pool for recal
# def readgroups_rec():
#     if (RECAL == "RG-file"):
#         return 'poolReadGroups=readgroups/{sample}.poolRG_recal.txt'
#     if (RECAL == "NO-file") and (SEQUENCE == "single"):
#         return 'poolReadGroups=readgroups/{sample}.poolRG_recal_fromSplit.txt'