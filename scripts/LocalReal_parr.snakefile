INTLIST=ch["int"]

rule CreateHeader:
    output:
        header="Results/2.LOCREAL/init/header.txt"
    shell:
        """
        line=$(grep -v Sample {config[sample_file]} | grep -v "#" | head -n1); sam=$(echo $line | awk '{{print $1}}') ; path=$(echo $line | awk '{{print $2}}')
        samtools view -H ${{path}}${{sam}}.bam | grep '^@SQ\|^@HD' > {output.header} 
        """


rule TargetCreatorL:
    input:
        bam=lambda wildcards: str(Path(BAM_PATH[wildcards.sample])/f"{wildcards.sample}.bam")
    output:
        interval=temp("Results/2.LOCREAL/Intervals/{sample}-{chrom}-.intervals")
    threads: config['threads']
    shell:
        """
        {config[GATK]}
        GenomeAnalysisTK \
           -T RealignerTargetCreator \
           -R {config[ref]} \
           -I {input.bam} \
           -L {wildcards.chrom} \
           -o {output} \
           -nt {config[threads]} \
           {config[known1]} {config[known2]}
        """

        
rule union1L:
    input:
        inter="Results/2.LOCREAL/Intervals/{sample}-{chrom}-.intervals", #$1
        header="Results/2.LOCREAL/init/header.txt"
    output:
        temp=temp("Results/2.LOCREAL/Intervals/{sample}-{chrom}-.interval_list"),
        temp1=temp("Results/2.LOCREAL/GuidanceIntervals/{sample}-{chrom}-.guidance.interval_list")
    params:
        IntFile=lambda wildcards: INTLIST[wildcards.chrom]
    shell:
        """
        cat {input.header} > {output.temp}
        sed -e "s/^{wildcards.chrom}://g" {input.inter} | awk -F '-' '{{if ($2) {{print "{wildcards.chrom}\t"$1"\t"$2"\t+\t{wildcards.chrom}:"$1"-"$2}} else {{print "{wildcards.chrom}\t"$1"\t"$1"\t+\t{wildcards.chrom}:"$1}} }}' >> {output.temp}
        set +e
        export _JAVA_OPTIONS='-Xmx120G'
        picard IntervalListTools \
            ACTION=UNION \
            SORT=true \
            UNIQUE=true \
            I={params.IntFile} \
            I={output.temp} \
            O={output.temp1}
        exitcode=$?
        if [ $exitcode -eq 1 ]; then exit 1; else exit 0; fi
        """

rule union2L:
    input:
        temp2="Results/2.LOCREAL/GuidanceIntervals/{sample}-{chrom}-.guidance.interval_list" #$2
    output:
        temp3=temp("Results/2.LOCREAL/GuidanceIntervals/{sample}-{chrom}-.guidance.interval_list2"),        
        final=temp("Results/2.LOCREAL/GuidanceIntervals/{sample}-{chrom}-.guidance.intervals")
    shell:
        """
        set +o pipefail
        sed '/@/d' {input.temp2} | grep "|" |  cut -f1-3|sed 's/\\t/:/'|sed 's/\\t/-/' >  {output.temp3}
        sed '/@/d' {input.temp2} | grep -v "|" |cut -f5|cat - {output.temp3} | sed 's/:/\\t/' |sed 's/-/\\t/'|sort -nk2,2 | sed 's/\\t/:/' | sed 's/\\t/-/' > {output.final}
        exitcode=$?
        if [ $exitcode -gt 1 ]; then exit 1; else exit 0; fi
        """


rule outlistL:
    output:
        namelistTemp=temp("Results/2.LOCREAL/lists/{sample}-{chrom}-.bamsGuidance"),
        namelist="Results/2.LOCREAL/lists/list_{sample}-{chrom}.map",
        guidanceList="Results/2.LOCREAL/lists/list_{sample}-{chrom}.guidance.list",
        placeholder="Results/2.LOCREAL/tmp/{sample}-{chrom}/{sample}-{chrom}-.txt"
    params:
        tmp="Results/2.LOCREAL/tmp/{sample}-{chrom}/",
        finalpath="Results/2.LOCREAL/PerChrom/",
        finalbam="{sample}-{chrom}-.bam"
    shell:
        """
        while read line; do
            if [[ "$(echo ${{line}} | awk '{{print $2}}')" == {wildcards.sample} ]]; then
                echo -e "Omitting line ${{line}} from guidance list for sample {wildcards.sample}"
            else
                bam=$(echo ${{line}} | awk '{{print $1}}')
                ls ${{bam}} >> {output.guidanceList}
                bamName=$(ls ${{bam}} | rev | cut -d/ -f1| rev) 
                echo -e "${{bamName}}\t{params.tmp}${{bamName}}" >> {output.namelistTemp}
            fi
        done < {config[bamsGuidance]}
        echo -e "{wildcards.sample}.bam\t{params.finalpath}{params.finalbam}"| cat {output.namelistTemp} - > {output.namelist}
        touch {output.placeholder}
        """

rule LocalRealignmentL:
    input:
        bam=lambda wildcards: str(Path(BAM_PATH[wildcards.sample])/f"{wildcards.sample}.bam"),
        guidanceList="Results/2.LOCREAL/lists/list_{sample}-{chrom}.guidance.list",
        intervals="Results/2.LOCREAL/GuidanceIntervals/{sample}-{chrom}-.guidance.intervals",
        paths="Results/2.LOCREAL/lists/list_{sample}-{chrom}.map"
    output:
        out=temp("Results/2.LOCREAL/PerChrom/{sample}-{chrom}-.bam"),
        bai=temp("Results/2.LOCREAL/PerChrom/{sample}-{chrom}-.bai")
    params:
        tmp="Results/2.LOCREAL/tmp/{sample}-{chrom}"
    shell:
        """
        {config[GATK]}
        GenomeAnalysisTK \
            -T IndelRealigner \
            -R {config[ref]} \
            -I {input.guidanceList} \
            -I {input.bam} \
            -targetIntervals {input.intervals} \
            -L {wildcards.chrom} \
            {config[known1]} {config[known2]} \
            -nWayOut {input.paths}
        echo "deleting temporary folder {params.tmp}"
        rm -r {params.tmp}
        """

rule mergeL:
    input:
        bams=expand("Results/2.LOCREAL/PerChrom/{{sample}}-{chrom}-.bam", chrom=CHROM)
    output:
        bam="Results/2.LOCREAL/final/{sample}.bam",
        bai="Results/2.LOCREAL/final/{sample}.bam.bai"
    shell:
        """
        samtools merge -c {output.bam} {input.bams}
        samtools index {output.bam}
        """
