

rule MergeReads:
    input:
        bam=lambda wildcards: str(Path(BAM_PATH[wildcards.sample])/f"{wildcards.sample}.bam")
    params:
        prefix="Results/3.ATLAS/mergedReads/{sample}",
        peRG=pairedRG()
    output:
        "Results/3.ATLAS/mergedReads/{sample}_mergedReads.bam",
        "Results/3.ATLAS/mergedReads/{sample}_mergedReads.bam.bai"
    message:
        "merging reads of paired end readgroups"
    conda:
        "environment2.yaml"
    shell:
        """
        test=$(cat {params.peRG})
        echo ${{test}}
        if [ -z ${{test}} ]; then pe=" " ; else in=$(cat {params.peRG}); pe=$(echo pairedReadGroups=${{in}}); fi
        {config[atlas]} task=mergeReads bam={input.bam} out={params.prefix} keepRandomRead ${{pe}}
        """


