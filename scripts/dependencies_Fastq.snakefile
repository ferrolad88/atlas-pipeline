##dependency-file for ATLAS workflow

#define variables
SEQUENCE = config['sequence']
FILE = df.index
SAMPLE= df.Sample
LIB= df['Lib']
FQ_PATH = df["Path"]
ADAPTERS = config["Adapter"]


#define input files for trimming
def trimmed():
    if (SEQUENCE == "single"):
        return ['Results/1.FASTQ/03.trimmed/{file}_R1_001_trimmed.fq.gz']
    else:
        return ['Results/1.FASTQ/03.trimmed/{file}_R1_001_val_1.fq.gz','Results/1.FASTQ/03.trimmed/{file}_R2_001_val_2.fq.gz']

#filter for mapped reads in single and paired end
def filterMap():
    if (SEQUENCE == "single"):
        return ['-F 4']
    else:
        return ['-f 3']