rule EstimateTheta:
    input:
        bam="Results/3.Perses/splitMerge/{sample}_mergedReads.bam",
        pmd="Results/3.Perses/PMDestimation/{sample}_PMD_input_Empiric.txt",
        recal="Results/3.Perses/recal/{sample}_recalibrationEM.txt"
    params:
        prefix="Results/4.Pallas/theta/wind/{sample}"
    output:
        "Results/4.Pallas/theta/wind/{sample}_theta_estimates.txt.gz"
    message:
        "Estimating theta for {wildcards.sample}...per window"
    conda:
        "environment2.yaml"
    shell:
        "{config[atlas]} task=theta bam={input.bam} pmdFile={input.pmd} recal={input.recal} out={params.prefix} {config[atlasParams]} {config[thetaWindowParams]}"

rule EstimateGlobalTheta:
    input:
        bam="Results/3.Perses/splitMerge/{sample}_mergedReads.bam",
        pmd="Results/3.Perses/PMDestimation/{sample}_PMD_input_Empiric.txt",
        recal="Results/3.Perses/recal/{sample}_recalibrationEM.txt"
    params:
        prefix="Results/4.Pallas/theta/genomeWide/{sample}",
        #recal=REC
    output:
        "Results/4.Pallas/theta/genomeWide/{sample}_theta_estimates.txt.gz"
    message:
        "Estimating genomeWide theta for {wildcards.sample}..."
    conda:
        "environment2.yaml"
    shell:
        "{config[atlas]} task=theta bam={input.bam} pmdFile={input.pmd} recal={input.recal} out={params.prefix} thetaGenomeWide {config[atlasParams]} {config[thetaGlobalParams]}"


rule getGlobalTheta:
    input:
        "Results/4.Pallas/theta/genomeWide/{sample}_theta_estimates.txt"
    output:
        dynamic("4.Pallas/analysis/theta/genomeWide/{sample}_Global_Theta.txt")
    shell:
        "cat {input} | grep genome-wide | cut -f11 > {output}"

