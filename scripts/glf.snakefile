rule GLF:
    input:
        bam="Results/3.Perses/splitMerge/{sample}_mergedReads.bam",
        pmd="Results/3.Perses/PMDestimation/{sample}_PMD_input_Empiric.txt",
        recal="Results/3.Perses/recal/{sample}_recalibrationEM.txt"
    params:
        prefix="Results/4.Pallas/GLF/{sample}"
    output:
        "Results/4.Pallas/GLF/{sample}.glf.gz"
    message:
        "GLF for ..... {wildcards.sample}..."
    conda:
        "environment2.yaml"
    shell:
        "{config[atlas]} task=GLF bam={input.bam} pmdFile={input.pmd} recal={input.recal} out={params.prefix} {config[atlasParams]} {config[glfParams]}"

