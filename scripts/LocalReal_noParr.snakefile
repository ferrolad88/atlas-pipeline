rule CreateHeader:
    output:
        header="Results/2.LOCREAL/init/header.txt"
    shell:
        """
        line=$(grep -v Sample {config[sample_file]} | grep -v "#" | head -n1); sam=$(echo $line | awk '{{print $1}}') ; path=$(echo $line | awk '{{print $2}}')
        samtools view -H ${{path}}${{sam}}.bam | grep '^@SQ\|^@HD' > {output.header} 
        """

rule TargetCreatorL:
    input:
        bam=lambda wildcards: str(Path(BAM_PATH[wildcards.sample])/f"{wildcards.sample}.bam")
    output:
        interval=temp("Results/2.LOCREAL/Intervals/{sample}.intervals")
    shell:
        """
        {config[GATK]}
        GenomeAnalysisTK \
           -T RealignerTargetCreator \
           -R {config[ref]} \
           -I {input.bam} \
           -o {output} \
           -nt 4 \
           {config[known1]} {config[known2]}
        """

        
rule union1L:
    input:
        sam_int="Results/2.LOCREAL/Intervals/{sample}.intervals", #$1
        target_int=int_list(),
        header="Results/2.LOCREAL/init/header.txt"
    output:
        temp=temp("Results/2.LOCREAL/Intervals/{sample}.interval_list"),
        temp1=temp("Results/2.LOCREAL/Intervals/{sample}.interval_list2"),
        temp2=temp("Results/2.LOCREAL/GuidanceIntervals/{sample}.guidance.interval_list")
    shell:
        """
        awk -F '-' 'BEGIN{{OFS = "-"}} {{print $1,$1,$2}}' {input.sam_int} | sed 's/-$//'|sed 's/-.*-/-/'|sed 's/-.*:/-/'|sed 's/:/\\t/' |sed 's/-/\\t/'|sed 's/$/\\t+/' > {output.temp}
        paste {output.temp} {input.sam_int} > {output.temp1}
        cat {input.header} {output.temp1} > {output.temp}
        set +e
        export _JAVA_OPTIONS='-Xmx120G'
        picard IntervalListTools \
            ACTION=UNION \
            SORT=true \
            UNIQUE=true \
            I={input.target_int}\
            I={output.temp} \
            O={output.temp2}
        exitcode=$?
        if [ $exitcode -eq 1 ]; then exit 1; else exit 0; fi
        """

rule union2L:
    input:
        temp2="Results/2.LOCREAL/GuidanceIntervals/{sample}.guidance.interval_list" #$2
    output:
        temp3=temp("Results/2.LOCREAL/GuidanceIntervals/{sample}.guidance.interval_list2"),        
        final=temp("Results/2.LOCREAL/GuidanceIntervals/{sample}.guidance.intervals")
    shell:
        """
        set +o pipefail
        sed '/@/d' {input.temp2} | grep "|" |  cut -f1-3|sed 's/\\t/:/'|sed 's/\\t/-/' >  {output.temp3}
        sed '/@/d' {input.temp2} | grep -v "|" |cut -f5|cat - {output.temp3} | sed 's/:/\\t/' |sed 's/-/\\t/'|sort -nk2,2 | sed 's/\\t/:/' | sed 's/\\t/-/' > {output.final}
        exitcode=$?
        if [ $exitcode -gt 1 ]; then exit 1; else exit 0; fi
        """


rule outlistL:
    output:
        namelistTemp=temp("Results/2.LOCREAL/lists/{sample}.bamsGuidance"),
        namelist="Results/2.LOCREAL/lists/list_{sample}.map",
        guidanceList="Results/2.LOCREAL/lists/list_{sample}.guidance.list",
        placeholder=temp("Results/2.LOCREAL/tmp/{sample}/{sample}-.txt")
    params:
        tmp="Results/2.LOCREAL/tmp/{sample}/",
        finalpath="Results/2.LOCREAL/final/",
        finalbam="{sample}.bam"
    shell:
        """
        while read line; do
            if [[ "$(echo ${{line}} | awk '{{print $2}}')" == {wildcards.sample} ]]; then
                echo -e "Omitting line ${{line}} from guidance list for sample {wildcards.sample}"
            else
                bam=$(echo ${{line}} | awk '{{print $1}}')
                ls ${{bam}} >> {output.guidanceList}
                bamName=$(ls ${{bam}} | rev | cut -d/ -f1| rev) 
                echo -e "${{bamName}}\t{params.tmp}${{bamName}}" >> {output.namelistTemp}
            fi
        done < {config[bamsGuidance]}
        echo -e "{wildcards.sample}.bam\t{params.finalpath}{params.finalbam}"| cat {output.namelistTemp} - > {output.namelist}
        touch {output.placeholder}
        """

rule LocalRealignmentL:
    input:
        bam=lambda wildcards: str(Path(BAM_PATH[wildcards.sample])/f"{wildcards.sample}.bam"),
        guidanceList="Results/2.LOCREAL/lists/list_{sample}.guidance.list",
        intervals="Results/2.LOCREAL/GuidanceIntervals/{sample}.guidance.intervals",
        paths="Results/2.LOCREAL/lists/list_{sample}.map"
    output:
        out="Results/2.LOCREAL/final/{sample}.bam",
        bai="Results/2.LOCREAL/final/{sample}.bai"
    params:
        tmp="Results/2.LOCREAL/tmp/{sample}"
    shell:
        """
        {config[GATK]}
        GenomeAnalysisTK \
            -T IndelRealigner \
            -R {config[ref]} \
            -I {input.guidanceList} \
            -I {input.bam} \
            -targetIntervals {input.intervals} \
            {config[known1]} {config[known2]} \
            -nWayOut {input.paths}
        echo "removing temporary files"
        rm -r {params.tmp}
        """


