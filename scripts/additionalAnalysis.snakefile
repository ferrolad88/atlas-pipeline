rule sex:
    input:
        bam="Results/3.Perses/splitMerge/{sample}_mergedReads.bam"
    params:
        prefix="Results/4.Pallas/sex/{sample}"
    output:
        "Results/4.Pallas/sex/{sample}.Sex"
    message:
        "Estimating the genetic sex with Skoglund(2013) on {wildcards.sample}"
    shell:
        "samtools view -q 30 {input.bam} | python scripts/yjasc_3752_ry_compute.py >> {params.prefix}.Sex "

