rule FileStats:
    input:
        bam="Results/1.FASTQ/08.MkDup_per_lib/{file}.Mkdup.bam",
        bai="Results/1.FASTQ/08.MkDup_per_lib/{file}.Mkdup.bam.bai"
    params:
        prefix="Results/1.FASTQ/x.FileStats/{file}"
    output:
        flag="Results/1.FASTQ/x.FileStats/{file}.Mkdup.flagstat",
        depth="Results/1.FASTQ/x.FileStats/{file}_approximateDepth.txt",
        stats="Results/1.FASTQ/x.FileStats/{file}_fragmentStats.txt",
        mq="Results/1.FASTQ/x.FileStats/{file}_MQ.txt",
        rl="Results/1.FASTQ/x.FileStats/{file}_readLength.txt",
        log="Results/1.FASTQ/x.FileStats/{file}_BamDiagnostics.log"
    message:
        "running BamDiagnostics...{wildcards.file}"
    shell:
        """
        samtools flagstat {input.bam} > {output.flag}
        {config[atlas]} task=BAMDiagnostics bam={input.bam} out={params.prefix} logFile={params.prefix}_BamDiagnostics.log verbose suppressWarnings
        """
rule SamStats:
    input:
        bam="Results/1.FASTQ/10.MkDup_per_sample/{sample}.Mkdup.bam",
        bai="Results/1.FASTQ/10.MkDup_per_sample/{sample}.Mkdup.bam.bai"
    params:
        prefix="Results/1.FASTQ/x.SamStats/{sample}"
    output:
        flag="Results/1.FASTQ/x.SamStats/{sample}.Mkdup.flagstat",
        depth="Results/1.FASTQ/x.SamStats/{sample}_approximateDepth.txt",
        stats="Results/1.FASTQ/x.SamStats/{sample}_fragmentStats.txt",
        mq="Results/1.FASTQ/x.SamStats/{sample}_MQ.txt",
        rl="Results/1.FASTQ/x.SamStats/{sample}_readLength.txt",
        log="Results/1.FASTQ/x.SamStats/{sample}_BamDiagnostics.log"
    message:
        "running BamDiagnostics...{wildcards.sample}"
    shell:
        """
        samtools flagstat {input.bam} > {output.flag}
        {config[atlas]} task=BAMDiagnostics bam={input.bam} out={params.prefix} logFile={params.prefix}_BamDiagnostics.log verbose suppressWarnings
        """



if (SEQUENCE == "single"):
    rule FileCounts:
        input:
            raw="Results/1.FASTQ/01.rawcount/{file}_R1_001.raw.readcount",
            #trimmed="Results/1.FASTQ/03.trimmed/{file}_R1_001.fastq.gz_trimming_report.txt",
            flag="Results/1.FASTQ/x.FileStats/{file}.Mkdup.flagstat",
            depth="Results/1.FASTQ/x.FileStats/{file}_approximateDepth.txt"
        params:
            trimmed="Results/1.FASTQ/03.trimmed/{file}_R1_001.fastq.gz_trimming_report.txt"
        output:
            "Results/1.FASTQ/y.FileCounts/{file}.counts"
        shell:
            """
            echo "Name Raw Trimmed AfterTrimming Aligned Duplicates Percent_Endogenous Percent_Dup Percent_Endog_NoDup Coverage " > {output}; 
            name={wildcards.file}
            echo -e "name = ${{name}}"
            raw=$(cat {input.raw})
            echo -e "raw = ${{raw}}"
            if [ -f {params.trimmed} ]; then trimmed=$(grep "length cutoff" {params.trimmed} | awk -F ":" '{{print $2}}' | awk -F "(" '{{print $1}}' | sed "s/ //g" | sed "s/\t//g"); else trimmed=0; fi
            echo -e "trimmed= ${{trimmed}}"
            afterTrim=$((${{raw}}-${{trimmed}}))
            echo -e "afterTrim = ${{afterTrim}}"
            aligned=$(cat {input.flag} | grep -m 1 "mapped" | awk '{{print $1}}')
            echo -e "aligned = ${{aligned}}"
            pcEndog=$(gawk -v aln="${{aligned}}" -v rw="${{raw}}" 'BEGIN {{OFMT="%.2f";print ((aln*100)/rw)}}')
            echo -e "pcEndog = ${{pcEndog}}"
            duplicates=$(cat {input.flag} | grep duplicates | awk '{{print $1}}')
            echo -e "duplicates = ${{duplicates}}"
            if [ ${{aligned}} == 0 ]; then echo "WARNING: ${{aligned}} sequences aligning to reference!"; pcDup=0; else pcDup=$(gawk -v dup="${{duplicates}}" -v aln="${{aligned}}" 'BEGIN {{OFMT="%.2f";print ((dup*100)/aln)}}'); fi
            echo -e "pcDup = ${{pcDup}}"
            pcEndogNoDup=$(gawk -v Dup="${{duplicates}}" -v rw="${{raw}}" -v aln="${{aligned}}" 'BEGIN {{OFMT="%.2f";print (((aln-Dup)*100)/rw)}}')
            echo -e "pcEndogNoDup = ${{pcEndogNoDup}}"
            cov=$(grep allReadGroups {input.depth} | awk '{{print $2}}');
            echo -e "cov = ${{cov}}"
            echo ${{name}} ${{raw}} ${{trimmed}} ${{afterTrim}} ${{aligned}} ${{duplicates}} ${{pcEndog}} ${{pcDup}} ${{pcEndogNoDup}} ${{cov}} >> {output}
            """
    rule FileCOUNT:
        input: 
            expand("Results/1.FASTQ/y.FileCounts/{file}.counts", zip, file=FILE)
        output: 
            "Results/1.FASTQ/FILE_COUNTS"
        shell:
            """
            echo "Name Raw Trimmed AfterTrimming Aligned Duplicates Percent_Endogenous Percent_Dup Percent_Endog_NoDup Coverage " > {output}; 
            cat {input} | grep -v "Name Raw Trimmed" >> {output}
            """

    rule SamCounts:
        input:
            cou=lambda wildcards: expand('Results/1.FASTQ/y.FileCounts/{file}.counts', file = df.index[ df.Sample == wildcards.sample]),
            flag='Results/1.FASTQ/x.SamStats/{sample}.Mkdup.flagstat',
            BAMD='Results/1.FASTQ/x.SamStats/{sample}_approximateDepth.txt'
        output: 
            "Results/1.FASTQ/y.SamCounts/{sample}.counts"
        shell:
            """
            echo "Name Raw(Sum) Trimmed(Sum) AfterTrimming(Sum) Aligned(Sum) Aligned Duplicates(Sum) Duplicates Percent_Endogenous Percent_Dup Percent_Endog_NoDup Coverage " > {output}; 
            name={wildcards.sample}
            raw=$(cat {input.cou} | grep -v "Name Raw" | awk '{{s+=$2}} END {{print s}}')
            trim=$(cat {input.cou} | grep -v "Name Raw" | awk '{{s+=$3}} END {{print s}}')
            afterTrim=$(cat {input.cou} | grep -v "Name Raw" | awk '{{s+=$4}} END {{print s}}')
            alignedSum=$(cat {input.cou} | grep -v "Name Raw" | awk '{{s+=$5}} END {{print s}}')
            duplicatesSum=$(cat {input.cou} |  grep -v "Name Raw" | awk '{{s+=$6}} END {{print s}}')
            aligned=$(cat {input.flag} | grep -m 1 "mapped" | awk '{{print $1}}')
            pcEndog=$(gawk -v aln="${{aligned}}" -v rw="${{raw}}" 'BEGIN {{OFMT="%.2f";print ((aln*100)/rw)}}')
            duplicates=$(cat {input.flag} | grep duplicates | awk '{{print $1}}')
            if [ ${{aligned}} == 0 ]; then echo "WARNING: ${{aligned}} sequences aligning to reference!"; pcDup=0; else pcDup=$(gawk -v dup="${{duplicates}}" -v aln="${{aligned}}" 'BEGIN {{OFMT="%.2f";print ((dup*100)/aln)}}');fi
            pcEndogNoDup=$(gawk -v Dup="${{duplicates}}" -v rw="${{raw}}" -v aln="${{aligned}}" 'BEGIN {{OFMT="%.2f";print (((aln-Dup)*100)/rw)}}')
            cov=$(grep allReadGroups {input.BAMD} | awk '{{print $2}}');
            echo ${{name}} ${{raw}} ${{trim}} ${{afterTrim}} ${{alignedSum}} ${{aligned}} ${{duplicatesSum}} ${{duplicates}} ${{pcEndog}} ${{pcDup}} ${{pcEndogNoDup}} ${{cov}} >> {output}
            """
    rule SamCOUNT:
        input: 
            expand("Results/1.FASTQ/y.SamCounts/{sample}.counts", zip, sample=SAMPLE)
        output: 
            "Results/1.FASTQ/SAM_COUNTS"
        shell:
            """
            echo "Name(Sum) Raw(Sum) Trimmed(Sum) AfterTrimming(Sum) Aligned(Sum) Aligned Duplicates(Sum) Duplicates Percent_Endogenous Percent_Dup Percent_Endog_NoDup Coverage " > {output}; 
            cat {input} | grep -v "Name(Sum) Raw(Sum) Trimmed(Sum)" >> {output}
            """



else:
    rule FileCounts:
        input:
            raw1="Results/1.FASTQ/01.rawcount/{file}_R1_001.raw.readcount",
            raw2="Results/1.FASTQ/01.rawcount/{file}_R2_001.raw.readcount",
            #trimmed="Results/1.FASTQ/03.trimmed/{file}_R2_001.fastq.gz_trimming_report.txt",
            flag="Results/1.FASTQ/x.FileStats/{file}.Mkdup.flagstat",
            depth="Results/1.FASTQ/x.FileStats/{file}_approximateDepth.txt"
        params:
            trimmed="Results/1.FASTQ/03.trimmed/{file}_R2_001.fastq.gz_trimming_report.txt",
        output:
            "Results/1.FASTQ/y.FileCounts/{file}.counts"
        shell:
            """
            echo "Name Raw1 Raw2 Raw Trimmed AfterTrimming Aligned Duplicates Percent_Endogenous Percent_Dup Percent_Endog_NoDup Coverage " > {output};             
            name={wildcards.file}         
            raw1=$(cat {input.raw1})
            raw2=$(cat {input.raw2})
            raw=$((${{raw1}}+${{raw2}}))
            if [ -f {params.trimmed} ]; then trimmed=$(grep "length cutoff" {params.trimmed} | awk -F ":" '{{print $2}}' | awk -F "(" '{{print $1}}' | sed "s/ //g" | sed "s/\t//g"); else trimmed=0; fi
            afterTrim=$((${{raw}}-${{trimmed}}))
            aligned=$(cat {input.flag} | grep -m 1 "mapped" | awk '{{print $1}}')
            pcEndog=$(gawk -v aln="${{aligned}}" -v rw="${{raw}}" 'BEGIN {{OFMT="%.2f";print ((aln*100)/rw)}}')
            duplicates=$(cat {input.flag} | grep duplicates | awk '{{print $1}}')
            if [ ${{aligned}} == 0 ]; then echo "WARNING: ${{aligned}} sequences aligning to reference!"; pcDup=0; else pcDup=$(gawk -v dup="${{duplicates}}" -v aln="${{aligned}}" 'BEGIN {{OFMT="%.2f";print ((dup*100)/aln)}}'); fi
            pcEndogNoDup=$(gawk -v Dup="${{duplicates}}" -v rw="${{raw}}" -v aln="${{aligned}}" 'BEGIN {{OFMT="%.2f";print (((aln-Dup)*100)/rw)}}')
            cov=$(grep allReadGroups {input.depth} | awk '{{print $2}}');
            echo -e "name = ${{name}}"
            echo -e "raw1 = ${{raw1}}"
            echo -e "raw2 = ${{raw2}}"
            echo -e "raw = ${{raw}}"
            echo -e "trimmed= ${{trimmed}}"
            echo -e "afterTrim = ${{afterTrim}}"
            echo -e "aligned = ${{aligned}}"
            echo -e "pcEndog = ${{pcEndog}}"
            echo -e "duplicates = ${{duplicates}}"
            echo -e "pcDup = ${{pcDup}}"
            echo -e "pcEndogNoDup = ${{pcEndogNoDup}}"
            echo -e "cov = ${{cov}}"
            echo ${{name}} ${{raw1}} ${{raw2}} ${{raw}} ${{trimmed}} ${{afterTrim}} ${{aligned}} ${{duplicates}} ${{pcEndog}} ${{pcDup}} ${{pcEndogNoDup}} ${{cov}} >> {output}
            """
    rule File_COUNT:
        input: 
            expand("Results/1.FASTQ/y.FileCounts/{file}.counts", zip, file=FILE)
        output: 
            "Results/1.FASTQ/FILE_COUNTS"
        shell:
            """
            echo "Name Raw1 Raw2 Raw Trimmed AfterTrimming Aligned Duplicates Percent_Endogenous Percent_Dup Percent_Endog_NoDup Coverage " > {output}; 
            cat {input} | grep -v "AfterTrimming" >> {output}
            """
           
    rule SamCounts:
        input:
            cou=lambda wildcards: expand('Results/1.FASTQ/y.FileCounts/{file}.counts', file = df.index[ df.Sample == wildcards.sample]),
            flag='Results/1.FASTQ/x.SamStats/{sample}.Mkdup.flagstat',
            BAMD='Results/1.FASTQ/x.SamStats/{sample}_approximateDepth.txt'
        output: 
            "Results/1.FASTQ/y.SamCounts/{sample}.counts"
        shell:
            """
            echo "Name(Sum) Raw(Sum) Trimmed(Sum) AfterTrimming(Sum) Aligned(Sum) Aligned Duplicates(Sum) Duplicates Percent_Endogenous Percent_Dup Percent_Endog_NoDup Coverage " > {output}
            name={wildcards.sample}
            echo "name = $name" 
            raw=$(cat {input.cou} | grep -v "Name Raw" | awk '{{s+=$4}} END {{print s}}')
            echo "raw = $raw" 
            trim=$(cat {input.cou} | grep -v "Name Raw" | awk '{{s+=$5}} END {{print s}}')
            echo -e "trim = $trim"
            aftertrim=$(cat {input.cou} | grep -v "Name Raw" | awk '{{s+=$6}} END {{print s}}')
            echo -e "aftertrim = $aftertrim"
            alignedSum=$(cat {input.cou} | grep -v "Name Raw" | awk '{{s+=$7}} END {{print s}}')
            echo -e "alignedSum = $alignedSum" 
            duplicatesSum=$(cat {input.cou} | awk '{{s+=$8}} END {{print s}}')
            echo -e "duplicatesSum = $duplicatesSum"
            aligned=$(cat {input.flag} | grep -m 1 "mapped" | awk '{{print $1}}')
            echo -e "aligned = $aligned"
            pcEndog=$(gawk -v aln="${{aligned}}" -v rw="${{raw}}" 'BEGIN {{OFMT="%.2f";print ((aln*100)/rw)}}')
            echo -e "pcEndog = $pcEndog"
            duplicates=$(cat {input.flag} | grep duplicates | awk '{{print $1}}')
            echo -e "duplicates = $duplicates" 
            if [ ${{aligned}} == 0 ]; then echo "WARNING: ${{aligned}} sequences aligning to reference!"; pcDup=0; else pcDup=$(gawk -v dup="${{duplicates}}" -v aln="${{aligned}}" 'BEGIN {{OFMT="%.2f";print ((dup*100)/aln)}}'); fi
            echo -e "pcDup = $pcDup"
            pcEndogNoDup=$(gawk -v Dup="${{duplicates}}" -v rw="${{raw}}" -v aln="${{aligned}}" 'BEGIN {{OFMT="%.2f";print (((aln-Dup)*100)/rw)}}')
            echo -e "pcEndogNoDup = $pcEndogNoDup" 
            cov=$(grep allReadGroups {input.BAMD} | awk '{{print $2}}');
            echo -e "cov = $cov"
            echo ${{name}} ${{raw}} ${{trim}} ${{aftertrim}} ${{alignedSum}} ${{aligned}} ${{duplicatesSum}} ${{duplicates}} ${{pcEndog}} ${{pcDup}} ${{pcEndogNoDup}} ${{cov}} >> {output}
            """

    rule SamCOUNT:
        input: 
            expand("Results/1.FASTQ/y.SamCounts/{sample}.counts", sample=SAMPLE)
        output: 
            "Results/1.FASTQ/SAM_COUNTS"
        shell:
            """
            echo "Name(Sum) Raw(Sum) Trimmed(Sum) AfterTrimming(Sum) Aligned(Sum) Aligned Duplicates(Sum) Duplicates Percent_Endogenous Percent_Dup Percent_Endog_NoDup Coverage " > {output}; 
            cat {input} | grep -v "Name(Sum) Raw(Sum) Trimmed(Sum)" >> {output}
            """
