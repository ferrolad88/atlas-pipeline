#def pmd_file():
#    if (PMD == "NO-PMD"):
#        return ['Results/3.Perses/blankPMD/{sample}_PMD_blank_Empiric.txt']
#    elif (PMD == "poolRG") or (PMD == "separate"):
#        return ['Results/3.Perses/PMDestimation/{sample}_PMD_input_Empiric.txt']

if (PMD == "F"):
    rule blankPMD:
        input:
            bam="Results/3.Perses/splitMerge/{sample}_mergedReads.bam"
        output:
            PMD="Results/3.Perses/PMDestimation/{sample}_PMD_input_Empiric.txt"
        message:
            "creating blank PMD values for {wildcards.sample}"
        shell:
            """
            rg=$(samtools view -H {input.bam} | grep @RG | awk '{{print $2}}'| cut -d: -f2-99)
            for i in ${{rg}}; do for j in $(echo -e "CT GA GT CA"); do echo -e "${{i}} ${{j}} Empiric[0]" >> {output.PMD}; done; done
            """

if (PMD == "T"):
    rule PMD_inputFile:
        params:
            splitmergeRG="readgroups/{sample}_readgroups.txt",
            poolRG="readgroups/{sample}.poolRG_pmd.txt"
        output:
            PMD_input="Results/3.Perses/readgroups/{sample}_PMD_inputfile.txt"
        shell:
            """
            if [[ -f "{params.poolRG}" ]]; then
                echo -e "reading readgroups to pool from {params.poolRG}"
                while read line; do 
                    echo $line >> {output.PMD_input}
                    newline=""
                    for i in ${{line}}; do
                        this=$(cat {params.splitmergeRG} | awk -v var="${{i}}" '{{if ($1==var && $2=="single" && ($3)) {{print $1"_truncated"}} }}')
                        newline="$newline $this"
                    done
                    if [[ $(echo $newline | wc -w) -gt 1 ]]; then echo $newline  >>  {output.PMD_input}; fi
                done < {params.poolRG}        
            else
                touch  {output.PMD_input}
            fi
            """



    rule PMDestimation:
        input:
            bam="Results/3.Perses/splitMerge/{sample}_mergedReads.bam",
            poolRG="Results/3.Perses/readgroups/{sample}_PMD_inputfile.txt"
        params:
            prefix="Results/3.Perses/PMDestimation/{sample}"
        output:
            "Results/3.Perses/PMDestimation/{sample}_PMD_input_Empiric.txt",
            "Results/3.Perses/PMDestimation/{sample}_PMD_input_Exponential.txt",
            "Results/3.Perses/PMDestimation/{sample}_PMD_Table_counts.txt",
            "Results/3.Perses/PMDestimation/{sample}_PMD_Table.txt"
        message:
            "PMD-estimation on Bamfile with split readgroups on {wildcards.sample}"
        shell:
            """
            if [ -s {input.poolRG} ]; then rg=$(echo "poolReadGroups={input.poolRG}"); else rg=$(echo " "); fi
            {config[atlas]} task=PMD bam={input.bam} fasta={config[ref]} ${{rg}} out={params.prefix} {config[atlasParams]} {config[pmdParams]}
            """
