rule callMLE:
    input:
        bam="Results/3.Perses/splitMerge/{sample}_mergedReads.bam",
        pmd="Results/3.Perses/PMDestimation/{sample}_PMD_input_Empiric.txt",
        recal="Results/3.Perses/recal/{sample}_recalibrationEM.txt"
    params:
        prefix="Results/4.Pallas/call/callMLE/{sample}",
    output:
        "Results/4.Pallas/call/callMLE/{sample}_MaximumLikelihood.vcf.gz"
    message:
        "MLE-caller...{wildcards.sample}..."
    shell:
        """
        {config[atlas]}  \
        task=call \
        method=MLE \
        fasta={config[ref]} \
        pmdFile={input.pmd} \
        recal={input.recal}  \
        bam={input.bam} \
        out={params.prefix} {config[atlasParams]}  {config[mleParams]} 
        """
 
rule AllelePresence:
    input:
        bam="Results/3.Perses/splitMerge/{sample}_mergedReads.bam",
        pmd="Results/3.Persess/PMDestimation/{sample}_PMD_input_Empiric.txt",
        recal="Results/3.Perses/recal/{sample}_recalibrationEM.txt"
    params:
        prefix="Results/4.Pallas/call/AllelePresence/{sample}"
    output:
        "Results/4.Pallas/call/AllelePresence/{sample}_AllelePresence.vcf.gz",
        "Results/4.Pallas/call/AllelePresence/{sample}_AllelePresence.log"
    message:
        "calling allele presence...{wildcards.sample}"
    conda:
        "environment2.yaml"
    shell:
        """
        {config[atlas]} \
        task=call \
        method=AllelePresence \
        bam={input.bam} \
        fasta={config[ref]} \
        pmdFile={input.pmd} \
        recal={input.recal} \
        out={params.prefix} {config[atlasParams]} {config[allelePresenceParams]}
        """

rule callBayes:
    input:
        bam="Results/3.Perses/splitMerge/{sample}_mergedReads.bam",
        pmd="Results/3.Perses/PMDestimation/{sample}_PMD_input_Empiric.txt",
        recal="Results/3.Perses/recal/{sample}_recalibrationEM.txt"
    params:
        prefix="Results/4.Pallas/call/callBayes/{sample}",
    output:
        "Results/4.Pallas/call/callBayes/{sample}_MaximumAPosteriori.vcf.gz"
    message:
        "Calling with beyesian method.....{wildcards.sample}"
    conda:
        "environment2.yaml"
    shell:
        """
        {config[atlas]} \
        task=call \
        method=Bayesian \
        bam={input.bam} \
        fasta={config[ref]} \
        pmdFile={input.pmd} \
        recal={input.recal}  \
        out={params.prefix} {config[atlasParams]} {config[bayesParams]}
        """
