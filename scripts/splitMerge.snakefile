

rule splitMerge:
    input:
        bam=lambda wildcards: str(Path(BAM_PATH[wildcards.sample])/f"{wildcards.sample}.bam"),
        bai=lambda wildcards: str(Path(BAM_PATH[wildcards.sample])/f"{wildcards.sample}.bam.bai"),
        readgroups="readgroups/{sample}_readgroups.txt"
    params:
        prefix="Results/3.Perses/splitMerge/{sample}"
    output:
        bam="Results/3.Perses/splitMerge/{sample}_mergedReads.bam",
        bai="Results/3.Perses/splitMerge/{sample}_mergedReads.bam.bai"
    message:
        "merging reads of paired end readgroups, splitting single-end readgroups of {wildcards.sample} ....."
    shell:
        """
        if [[  -f "{input.readgroups}" ]]; then
            echo -e "found readgroup file {input.readgroups}"
            {config[atlas]} task=splitMerge bam={input.bam} out={params.prefix} readGroupSettings={input.readgroups} allowForLarger {config[atlasParams]}  {config[recalBamParams]}
        else
            echo -e "no readgroup file found for {wildcards.sample}. No readgroups will be merged or split. copying input bam to {output.bam}"
            cp {input.bam} {output.bam}
        fi
        """
