if (ADAPTERS == "T"):
    rule align:
        input:
            trimmed()
        output:
            temp("Results/1.FASTQ/04.alignments/{file}_aligned.bam")
        threads: config['threads']
        conda:
            "environment2.yaml"
        shell:
            """
            bwa mem -t {threads} -M {config[ref]} {input} | samtools view -bSh -q {config[mappingqual]} - > {output}
            """
if (ADAPTERS == "F"):
    if (SEQUENCE == "single"):
        rule align:
            input:
                lambda wildcards: str(Path(FQ_PATH[wildcards.file]) / f"{wildcards.file}_R1_001.fastq.gz")
            output:
                temp("Results/1.FASTQ/04.alignments/{file}_aligned.bam")
            threads: 16
            conda:
                "environment2.yaml"
            shell:
                """
                bwa mem -t 8 -M {config[ref]} {input} | samtools view -bSh -q {config[mappingqual]} - > {output}
                """    
    else:
        rule align:
            input:
                lambda wildcards: str(Path(FQ_PATH[wildcards.file]) / f"{wildcards.file}_R1_001.fastq.gz"), 
                lambda wildcards: str(Path(FQ_PATH[wildcards.file]) / f"{wildcards.file}_R2_001.fastq.gz")
            output:
                temp("Results/1.FASTQ/04.alignments/{file}_aligned.bam")
            threads: 16
            conda:
                "environment2.yaml"
            shell:
                """
                bwa mem -t 8 -M {config[ref]} {input} | samtools view -bSh -q {config[mappingqual]} - > {output}
                """    


rule sort_index:
    input:
        "Results/1.FASTQ/04.alignments/{file}_aligned.bam"
    output:
        sort=temp("Results/1.FASTQ/05.sort/{file}_sort.bam"),
        index=temp("Results/1.FASTQ/05.sort/{file}_sort.bam.bai")
    params:
        prefix="Results/1.FASTQ/05.sort/{file}"
    shell:
        """
        rm -f {params.prefix}.bam.tmp.*.bam 
        samtools sort {input} -o {output.sort}
        samtools index {output.sort}
        """

rule newhead:
    input:
        bam="Results/1.FASTQ/05.sort/{file}_sort.bam",
        bai="Results/1.FASTQ/05.sort/{file}_sort.bam.bai"
    params:
        ID="{file}",
        LB=lambda wildcards: LIB[wildcards.file],
        SM=lambda wildcards: SAMPLE[wildcards.file]
    output:
        bam=temp("Results/1.FASTQ/06.newhead/{file}_newhead.bam")
    shell:
        """
        set +e
        echo -e "RGSM = {params.SM}"
        FASTQHEAD=$(samtools view {input.bam} | head -1 | sed 's/" "/-/g' | awk '{{print $1}}' | cut -d ":" -f 1,2,3,4);
        picard AddOrReplaceReadGroups \
        I={input.bam} \
        O={output.bam} \
        ID={params.ID} \
        LB={params.LB} \
        PL=Illumina \
        PU=${{FASTQHEAD}} \
        SM={params.SM} \
        CN={config[CN]}
        exitcode=$?
        if [ $exitcode -eq 1 ]; then exit 1; else exit 0; fi
        """

rule filtering:
    input:
        bam="Results/1.FASTQ/06.newhead/{file}_newhead.bam"
    params:
        filter_mapped=filterMap()
    output:
        primary=temp("Results/1.FASTQ/07.Filtered/{file}_primary.bam"),
        mapped=temp("Results/1.FASTQ/07.Filtered/{file}_filtered.bam"),
        index=temp("Results/1.FASTQ/07.Filtered/{file}_filtered.bam.bai")
    shell:
        """
        samtools view -bh -F 256 {input} > {output.primary}
        samtools view -bh {params.filter_mapped} {output.primary} > {output.mapped}
        samtools index {output.mapped}
        """    



rule MkDup_File:
    input:
        "Results/1.FASTQ/07.Filtered/{file}_filtered.bam"
    output:
        bam=temp("Results/1.FASTQ/08.MkDup_per_lib/{file}.Mkdup.bam")
    shell:
        """
        export _JAVA_OPTIONS='{config[Xmx]}'
        set +e
        picard MarkDuplicates INPUT={input} OUTPUT={output.bam} METRICS_FILE=/dev/null REMOVE_DUPLICATES=false AS=true VALIDATION_STRINGENCY=SILENT TMP_DIR=tmp/ TAGGING_POLICY=All
        exitcode=$?
        if [ $exitcode -eq 1 ]; then exit 1; else exit 0; fi
        """


rule MkDup_index1:
    input:
        bam="Results/1.FASTQ/08.MkDup_per_lib/{file}.Mkdup.bam"
    output:
        index="Results/1.FASTQ/08.MkDup_per_lib/{file}.Mkdup.bam.bai"
    shell:
        """
        samtools index {input.bam}
        """


