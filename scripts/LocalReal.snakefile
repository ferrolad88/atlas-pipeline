INTLIST=ch["int"]


rule TargetCreatorL:
    input:
        bam=lambda wildcards: str(Path(BAM_PATH[wildcards.sample])/f"{wildcards.sample}.bam")
    output:
        interval=temp("Results/2.LOCREAL/Intervals/{sample}-t-{chrom}-c-.intervals")
    shell:
        """
        {config[GATK]}
        GenomeAnalysisTK \
           -T RealignerTargetCreator \
           -R {config[ref]} \
           -I {input.bam} \
           -L {wildcards.chrom} \
           -o {output} \
           -nt 4 \
           {config[known1]} {config[known2]}
        """

        
rule union1L:
    input:
        "Results/2.LOCREAL/Intervals/{sample}-t-{chrom}-c-.intervals" #$1
    output:
        temp=temp("Results/2.LOCREAL/Intervals/{sample}-t-{chrom}-c-.interval_list"),
        temp1=temp("Results/2.LOCREAL/Intervals/{sample}-t-{chrom}-c-.interval_list2"),
        temp2=temp("Results/2.LOCREAL/GuidanceIntervals/{sample}-t-{chrom}-c-.guidance.interval_list")
    params:
        IntFile=lambda wildcards: INTLIST[wildcards.chrom]
    shell:
        """
        awk -F ':' '{{print $1,$2}}' {input} | awk -v OFS='\t' '{{if($2 ~ "-"){{split($2,a,/-/); print $1,a[1],a[2],"+"}}else{{print $1,$2,$2,"+"}} }}' > {output.temp}
        paste {output.temp} {input} > {output.temp1}
        cat {config[header]} {output.temp1} > {output.temp}
        set +e
        export _JAVA_OPTIONS='-Xmx120G'
        picard IntervalListTools \
            ACTION=UNION \
            SORT=true \
            UNIQUE=true \
            I={params.IntFile} \
            I={output.temp} \
            O={output.temp2}
        exitcode=$?
        if [ $exitcode -eq 1 ]; then exit 1; else exit 0; fi
        """

rule union2L:
    input:
        temp2="Results/2.LOCREAL/GuidanceIntervals/{sample}-t-{chrom}-c-.guidance.interval_list" #$2
    output:
        temp3=temp("Results/2.LOCREAL/GuidanceIntervals/{sample}-t-{chrom}-c-.guidance.interval_list2"),        
        final=temp("Results/2.LOCREAL/GuidanceIntervals/{sample}-t-{chrom}-c-.guidance.intervals")
    shell:
        """
        set +o pipefail
        sed '/@/d' {input.temp2} | grep "|" |  cut -f1-3|sed 's/\\t/:/'|sed 's/\\t/-/' >  {output.temp3}
        sed '/@/d' {input.temp2} | grep -v "|" |cut -f5|cat - {output.temp3} | sed 's/:/\\t/' |sed 's/-/\\t/'|sort -nk2,2 | sed 's/\\t/:/' | sed 's/\\t/-/' > {output.final}
        exitcode=$?
        if [ $exitcode -gt 1 ]; then exit 1; else exit 0; fi
        """
if not (GUIDANCE == "F"):

    rule outlistL:
        output:
            bamsGuidance="Results/2.LOCREAL/lists/{sample}-{chrom}-.bamsGuidance",
            namelist="Results/2.LOCREAL/lists/list_{sample}-{chrom}.map",
            guidance="Results/2.LOCREAL/tmp/{sample}-{chrom}/{sample}-{chrom}-.guidance.list"
        params:
            tmp="Results/2.LOCREAL/tmp/{sample}-{chrom}/",
            finalpath="Results/2.LOCREAL/PerChrom/",
            finalbam="{sample}-{chrom}-.bam"
        shell:
            """
            while read line; do 
            sam=$(echo $line | awk '{{print $1}}'); 
                if [ "$sam" == "{wildcards.sample}" ]; then; 
                    echo "ignoring $line in guidance list"; 
                else echo $line | awk '{{print $2}}' >> {output.guidance} 
                fi;
            done < {config[bamsGuidance]}
            for i in $(cat {output.guidance); do bam=$(ls ${{i}} | rev | cut -d/ -f1| rev); echo -e "${{bam}}\t{params.tmp}${{bam}}" >> {output.bamsGuidance}; done 
            echo -e "{wildcards.sample}.bam\t{params.finalpath}{params.finalbam}"| cat {output.bamsGuidance} - > {output.namelist}
            """



    rule LocalRealignmentL:
        input:
            bam=lambda wildcards: str(Path(BAM_PATH[wildcards.sample])/f"{wildcards.sample}.bam"),
            intervals="Results/2.LOCREAL/GuidanceIntervals/{sample}-{chrom}-.guidance.intervals",
            paths="Results/2.LOCREAL/lists/list_{sample}-{chrom}.map",
            guidance="Results/2.LOCREAL/tmp/{sample}-{chrom}/{sample}-{chrom}-.guidance.list"
        output:
            out=temp("Results/2.LOCREAL/PerChrom/{sample}-{chrom}-.bam"),
            bai=temp("Results/2.LOCREAL/PerChrom/{sample}-{chrom}-.bai"),
            placeholder=tmp("Results/2.LOCREAL/tmp/{sample}-{chrom}/{sample}-{chrom}.txt")
        shell:
            """
            {config[GATK]}
            GenomeAnalysisTK \
                -T IndelRealigner \
                -R {config[ref]} \
                -I {input.guidance}
                -I {input.bam} \
                -targetIntervals {input.intervals} \
                -L {wildcards.chrom} \
                {config[known1]} {config[known2]} \
                -nWayOut {input.paths}
            """
else:
    rule LocalRealignmentL:
        input:
            bam=lambda wildcards: str(Path(BAM_PATH[wildcards.sample])/f"{wildcards.sample}.bam"),
            intervals="Results/2.LOCREAL/GuidanceIntervals/{sample}-{chrom}-.guidance.intervals",
        output:
            out=temp("Results/2.LOCREAL/PerChrom/{sample}-{chrom}-.bam"),
            bai=temp("Results/2.LOCREAL/PerChrom/{sample}-{chrom}-.bai"),
        shell:
            """
            {config[GATK]}
            GenomeAnalysisTK \
                -T IndelRealigner \
                -R {config[ref]} \
                -I {input.bam} \
                -targetIntervals {input.intervals} \
                -L {wildcards.chrom} \
                {config[known1]} {config[known2]} \
                -O {output.out}
            """



rule mergeL:
    input:
        bams=expand("Results/2.LOCREAL/PerChrom/{{sample}}-t-{chrom}-c-.bam", chrom=CHROM)
    output:
        bam="Results/2.LOCREAL/final/{sample}-t.bam",
        bai="Results/2.LOCREAL/final/{sample}-t.bam.bai"
    shell:
        """
        samtools merge -c {output.bam} {input.bams}
        samtools index {output.bam}
        """
